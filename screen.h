/**********************************************************************
  Module: screen.h
  Author: Jim Uhl
  Date:   2004 October 02

  Purpose: Draw 2d images on the screen.  Also, draw main game image.

  Modifier: Scott Wang
  History: add screen_check which check for the initial screen size
           add screen_draw_integer that be able to print an integer on
           the screen at given poistion  
**********************************************************************/

#ifndef SCREEN_H
#define SCREEN_H

/*** Screen limits ***/

/* width */
#define SCR_LEFT 0
#define SCR_WIDTH 80
#define SCR_RIGHT (SCR_LEFT+SCR_WIDTH-1)

/* height */
#define SCR_TOP 0
#define SCR_HEIGHT 24
#define SCR_BOTTOM (SCR_TOP+SCR_HEIGHT-1)

/* Screen coordinates of the playfield origin. */
#define SCR_FIELD_OFFSET_ROW 1
#define SCR_FIELD_OFFSET_COL 7

/* Screen coordinates of the time clock. */
#define SCR_TIMER_ROW 5
#define SCR_TIMER_COL 62

/* Screen coordinates of the lives counter. */
#define SCR_LIVES_ROW 7
#define SCR_LIVES_COL 63

/* Initialize curses, draw initial gamescreen. Refreshes screen to terminal. */
extern void screen_init(void);

/* Draw initial game board. */
extern void screen_draw_board(void);

/* Draws 2d `image' of `height' rows, at curses coordinates `(row, col)'.
   Note: parts of the `image' falling on negative rows are not drawn; each
   row drawn is clipped on the left and right side of the game screen (note
   that `col' may be negative, indicating `image' starts to the left of the
   screen and will thus only be partially drawn.  */
extern void screen_draw_image(int row, int col, char *image[], int height);

/* Clears a 2d `width'x`height' rectangle with spaces.  Upper left hand
   corner is curses coordinate `(row,col)'. */
extern void screen_clear_image(int row, int col, int width, int height);

/* Moves cursor to bottom right corner and refreshes. */
extern void screen_refresh(void);

/* Turns BOLD mode ON. */
extern void screen_bold_on(void);

/* Turns BOLD mode OFF. */
extern void screen_bold_off(void);

/* Terminates curses cleanly. */
extern void screen_fini(void);

/* check if the init screen size */
extern void screen_check(void);

/* print an integer in a given posistion */
extern void screen_draw_integer(int row, int col, int i);

#endif /* SCREEN_H */
