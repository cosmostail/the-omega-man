/**********************************************************************
   Module: eheap.c
   Author: Scott G Wang
   Date:   2006 Sep 16
   Purpose: Malloc a spaces..
   Modification History:
**********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "eheap.h"

void *emalloc(size_t nbytes)
{
  void *p = malloc(nbytes); 
  if (p == NULL) {
    perror("Malloc");
    exit(1);
  }
  return p;
}

void efree(void *p)
{
  free(p);
}
