/**********************************************************************
  Module: collision.c
  Author: Scott Wang
  Date:   2006 Oct 16
  Purpose: define all the collision events
**********************************************************************/
#include <assert.h>
#include <stdio.h>
#include "collision.h"
#include "move_man.h"
#include "move_zombie.h"
#include "move_bullet.h"

/* function pointer */
typedef void (* Action)(void *, void *);

/* zombie moves to man, man die */
static void collision_zombie_man_killed(void *from_data,void *to_data);

/* man moves to zombie, man die */
static void collision_man_zombie_killed(void *from_data,void *to_data);

/* man's bullet moves to zombie */
static void collision_zombie_killed(void *from_data, void *to_data);

/* zombie moves to man's bullet */
static void collision_zombie_man_bullet(void *from_data,void *to_data);

/* zombie's bullet moves to  man */
static void collision_man_killed(void *from_data, void *to_data);

/* man moves to zombie's bullet */
static void collision_man_zombie_bullet(void *from_data, void *to_data);

/* zombie shot zombie, bullet cancel */
static void collision_bullet_zombie_cancel(void *from_data, void *to_data);

/* zombie moves to zombies's bullet, bullet cancel */
static void collision_zombie_bullet_cancel(void *from_data, void *to_data);

/* bullet moves to bullet, cancel each other */
static void collision_bullet_bullet_cancel(void *from_data, void *to_data);



/* define the collision detection and corresponding action */

Action c_type[GAME_TYPE_LAST][GAME_TYPE_LAST] = {
  {
   /* nothing conflict empty to */
    NULL,
    NULL,
    NULL,
    NULL,
    NULL,
  },

  //man
  {
    NULL, //man to empty
    NULL,  //man to man
    collision_man_zombie_killed, //man to zombie
    collision_man_zombie_bullet, //man to zombie's bullet
    NULL,//man to man's bullet, not define
  },

  //zombie
  {
    NULL, //zombie to empty
    collision_zombie_man_killed, //zombie to man
    NULL, //zombie to zombie
    collision_zombie_bullet_cancel, //zombie to zombie's bullet
    collision_zombie_man_bullet, //zombie to man's bullet
  },

  //zombie's bullet
  {
    NULL, //zombie's bullet to empty
    collision_man_killed, //zombie's bullet to man
    collision_bullet_zombie_cancel, //zombie's bullet to zombie
    collision_bullet_bullet_cancel, //zombie's bullet to zombie's bullet
    collision_bullet_bullet_cancel, //zombie's bullet to man's bullet
  },

  //man's bullet
  {
    NULL, //man's bullet to empty
    NULL, //man's bullet to man
    collision_zombie_killed, //man's bullet to zombie
    collision_bullet_bullet_cancel, //man's bullet to zombie's bullet
    collision_bullet_bullet_cancel, //man's bullet to man's bullet
  },
};


int collision_is_occur(game_type_t from, void *from_data,
		       game_type_t to, void *to_data)
{
  assert(from < GAME_TYPE_LAST && from >= 0 &&
	 to < GAME_TYPE_LAST && to >=0);

  if (c_type[from][to]  == NULL){
    return 0;
  }
  
  c_type[from][to](from_data,to_data);
  return 1;
}

void collision_zombie_man_killed(void *from_data,void *to_data)
{
  move_man_killed(to_data);
}

void collision_man_zombie_killed(void *from_data,void *to_data)
{
  move_man_killed(from_data);
}


void collision_zombie_killed(void *from_data, void *to_data)
{
  move_zombie_killed(to_data);
  move_bullet_killed(from_data); 
}

void collision_man_killed(void *from_data, void *to_data)
{
  move_man_killed(to_data);
  move_bullet_killed(from_data); 
}

void collision_man_zombie_bullet(void *from_data, void *to_data)
{
  move_bullet_killed(to_data);
  move_man_killed(from_data);
}

void collision_zombie_man_bullet(void *from_data, void *to_data)
{
  move_zombie_killed(from_data);
  move_bullet_killed(to_data);
}

void collision_bullet_zombie_cancel(void *from_data, void *to_data)
{
  move_bullet_killed(from_data);
}

void collision_zombie_bullet_cancel(void *from_data, void *to_data)
{
  move_bullet_killed(to_data);
}

void collision_bullet_bullet_cancel(void *from_data, void *to_data)
{
  move_bullet_killed(from_data);
  move_bullet_killed(to_data);
}
