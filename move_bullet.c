/**********************************************************************
  Module: move_bullet.c
  Author: Scott Wang
  Date:   2006 October 07
  Purpose: move bullet 
**********************************************************************/
#include <assert.h>
#include <unistd.h>

#include "object_count.h"
#include "globals.h"
#include "ethread.h"
#include "move_bullet.h"
#include "screen.h"
#include "eheap.h"
#include "image.h"
#include "field.h"

#define USED 0
#define UNUSE 1

/* a data structure for bullet
   status indicate wether a bullet is used or not
   a bullet is used iff it shots a zombie*/
typedef struct bullet_args {
  int row;
  int col;
  dir_t dir;
  int status;
  game_type_t type;
  pthread_t thread;
}*Bullet_args;

static void move_bullet_draw_image(Bullet_args b);
static void move_bullet_clear_image(Bullet_args b);
static void move_bullet_exit(Bullet_args b);

void move_bullet_create(int row, int col, dir_t dir, game_type_t t )
{
  int tmp_row = row;
  int tmp_col = col;

  dir_move(dir,&tmp_row,&tmp_col);
  
  if(FIELD_IS_IN_PLAY(tmp_row,tmp_col) 
     && g_adt_get_type(tmp_row,tmp_col) == EMPTY){

    pthread_mutex_lock(&malloc_mutex);
    Bullet_args tmp = (Bullet_args) emalloc(sizeof(struct bullet_args));
    pthread_mutex_unlock(&malloc_mutex);

    tmp->row = tmp_row;
    tmp->col = tmp_col;
    tmp->dir = dir;
    tmp->status = UNUSE;
    tmp->type = t;

    move_bullet_draw_image(tmp);

    g_adt_lock(tmp_row,tmp_col);
      g_adt_add(tmp_row,tmp_col,tmp,t);
    g_adt_unlock(tmp_row,tmp_col);

    /* increase objects number */
    object_count_inc();
    
    ethread_create(&tmp->thread,NULL,move_bullet,tmp);
  }
}

void move_bullet(void *data)
{
  Bullet_args b = (Bullet_args) data;
  assert(b != NULL);

  while(1){
    usleep(BULLET_PRE_MOVE);

    move_bullet_clear_image(b);

    if(reset_flag){
      sem_post(&num_of_objects);
      move_bullet_exit(b);
    }

    int tmp_row = b->row;
    int tmp_col = b->col;

    /* lock global mutex for whole field
       then lock current cell
       get the next row, col
       lock where it goes    
       unlock the global mutex */
    pthread_mutex_lock(&g_adt_mutex);
      g_adt_lock(tmp_row,tmp_col);
      dir_move(b->dir,&b->row,&b->col);
      g_adt_lock(b->row,b->col);
    pthread_mutex_unlock(&g_adt_mutex);

    g_adt_remove(tmp_row,tmp_col);
  
    if(FIELD_IS_IN_PLAY(b->row,b->col) && b->status == UNUSE){
      move_bullet_draw_image(b);
      g_adt_add(b->row,b->col,b,b->type);
      /* unlock the cells */
      g_adt_unlock(b->row,b->col);
      g_adt_unlock(tmp_row,tmp_col);
    }else{
      g_adt_unlock(b->row,b->col);
      g_adt_unlock(tmp_row,tmp_col);

      /* decrease counter */
      object_count_dec();

      move_bullet_exit(b);
    }
  }
}

void move_bullet_clear_image(Bullet_args b)
{
  assert(b != NULL);
  pthread_mutex_lock(&curses_mutex);
  screen_clear_image(b->row,b->col,BULLET_WIDTH,BULLET_HEIGHT);
  screen_refresh();
  pthread_mutex_unlock(&curses_mutex);
}

void move_bullet_draw_image(Bullet_args b)
{
    assert(b != NULL);
   
    char *b_img = BULLET_IMAGE;

    pthread_mutex_lock(&curses_mutex);
    screen_draw_image(b->row,b->col,&b_img,BULLET_HEIGHT);
    screen_refresh();
    pthread_mutex_unlock(&curses_mutex);
}

void move_bullet_killed(void *data)
{
  Bullet_args b = (Bullet_args) data;
  b->status = USED;
  g_adt_remove(b->row,b->col);
  move_bullet_clear_image(b);
}

void move_bullet_exit(Bullet_args b)
{
  pthread_t tmp = b->thread;
  efree(b);
  ethread_exit(&tmp);
}
