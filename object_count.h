/**********************************************************************
  Module: object_count.h
  Author: Scott Wang
  Date:   2006 Nov 4
  Purpose: a counter that counts the objects numbers on screen
********************************************************************/

#ifndef OBJECT_COUNT_H
#define OBJECT_COUNT_H

/* increase object counter */
extern void object_count_inc();

/* decrease object counter */
extern void object_count_dec();

/* init object counter */
extern void object_count_init();

/* get object counter number */
extern int object_count_get();

#endif
