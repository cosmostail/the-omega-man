/**********************************************************************
  Module: move_man.c
  Author: Scott Wang
  Date:   2006 Sep 29

  Purpose: To make man move in the given field
**********************************************************************/
#include <assert.h>
#include <stdlib.h>
#include <unistd.h>

#include "globals.h"
#include "screen.h"
#include "move_man.h"
#include "eheap.h"
#include "field.h"
#include "g_adt.h"
#include "image.h"
#include "ethread.h"

#define ALIVE 1
#define DEAD 0

struct man_args {
  int row;
  int col;
  dir_t dir;
  int status;
  pthread_t pthread;
};

static pthread_mutex_t man_mutex = PTHREAD_MUTEX_INITIALIZER;
volatile int man_live = MAN_LIVE;

static void move_man_init_args(Man_args m);
static void move_man_draw_image(Man_args m);
static void move_man_clear_image(Man_args m);
static void move_man_draw_live(int live);

Man_args move_man_args_create()
{
  pthread_mutex_lock(&malloc_mutex);
  Man_args tmp = (Man_args) emalloc(sizeof(struct man_args));
  pthread_mutex_unlock(&malloc_mutex);
  
  move_man_init_args(tmp);
  ethread_create(&tmp->pthread,NULL,move_man,tmp); 
  return tmp;
}

void move_man_init_args(Man_args m)
{
  assert(m != NULL);
  m->row = FIELD_MAX_ROW;
  m->col = FIELD_MAX_COL;
  m->dir = DIR_WEST;
  m->status = ALIVE;
  move_man_draw_image(m);
  move_man_draw_live(man_live);
}

void move_man(void *data)
{
  Man_args m = (Man_args) data;
  assert(m != NULL);
  
  while(1){
    usleep(MAN_PRE_MOVE);
    if(m->status == DEAD){
      sem_wait(&reset_done);
      move_man_init_args(m);
    }

    /* lock man up */
    pthread_mutex_lock(&man_mutex);

    /* clear man image */
    move_man_clear_image(m);
    
    /* get a copy of current row, col */
    int tmp_row = m->row;
    int tmp_col = m->col;

    /* lock global mutex for whole field */
    pthread_mutex_lock(&g_adt_mutex);
       /* lock current cell */
       g_adt_lock(tmp_row,tmp_col);
       /* get the next row, col */ 
       dir_move(m->dir,&m->row,&m->col);
       /* lock where it goes */
       g_adt_lock(m->row,m->col);
    /*unlock the global mutex */
    pthread_mutex_unlock(&g_adt_mutex);
       

    /* if man is in the field,man move */
    if(FIELD_IS_IN_PLAY(m->row,m->col) 
        && (g_adt_get_type(m->row,m->col) == EMPTY 
	    ||  g_adt_get_type(m->row,m->col) == ZOMBIE)){ 
      g_adt_remove(tmp_row,tmp_col);
      g_adt_add(m->row,m->col,m,MAN);
      
      g_adt_unlock(m->row,m->col);
      g_adt_unlock(tmp_row,tmp_col);
    }else{
      g_adt_unlock(m->row,m->col);
      g_adt_unlock(tmp_row,tmp_col);
      
      m->row = tmp_row;
      m->col = tmp_col;
    }
    
    move_man_draw_image(m);
    
    pthread_mutex_unlock(&man_mutex);
  }
}

void move_man_clear_image(Man_args m)
{
  assert(m != NULL);
  pthread_mutex_lock(&curses_mutex);
  screen_clear_image(m->row,m->col,MAN_WIDTH,MAN_HEIGHT);
  pthread_mutex_unlock(&curses_mutex);
}

void move_man_draw_image(Man_args m)
{
    assert(m != NULL);
    
    char *m_img = man(m->dir);
    
    pthread_mutex_lock(&curses_mutex);    
    screen_bold_on();
    screen_draw_image(m->row, m->col,&m_img,MAN_HEIGHT);
    screen_refresh();
    screen_bold_off();
    pthread_mutex_unlock(&curses_mutex);
}

void move_man_change_pos(Man_args m, dir_t dir)
{
  pthread_mutex_lock(&man_mutex);
  if(dir_is_legal(m->row,m->col,dir))
    m->dir = dir;
  pthread_mutex_unlock(&man_mutex);
}

void move_man_draw_live(int live)
{
  pthread_mutex_lock(&curses_mutex);
  screen_draw_integer(SCR_LIVES_ROW,SCR_LIVES_COL,live);
  screen_refresh();
  pthread_mutex_unlock(&curses_mutex);
}

void move_man_killed(void *data)
{
  Man_args m = (Man_args) data;

  if (--man_live > 0){
    g_adt_remove(m->row,m->col);
    move_man_clear_image(m);
    reset_flag = 1;
    sem_post(&need_to_reset);
    m->status = DEAD;
   }else{
    g_adt_lose();
  }
}


int move_man_get_row(Man_args m)
{
  assert(m != NULL);
  return m->row;
}

int move_man_get_col(Man_args m)
{
  assert(m != NULL);
  return m->col;
}

dir_t move_man_get_dir(Man_args m)
{
  assert(m != NULL);
  return m->dir;
}

void move_man_lock()
{
  pthread_mutex_lock(&man_mutex);
}

void move_man_unlock()
{
  pthread_mutex_unlock(&man_mutex);
}
