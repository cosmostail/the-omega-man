/**********************************************************************
  Module: man.c
  Author: Scott Wang
  Date:   2006 Sep 29

  Purpose: Create Man image
**********************************************************************/

#include <stdlib.h>
#include <assert.h>
#include "image.h"

static char *man_poses[DIR_NUM] = { "i", "!","{","}"};
static char *zombie_poses[DIR_NUM] = { "^", "v","<",">"};

char *man(dir_t p)
{
  assert(DIR_OKAY(p));
  return man_poses[p];
}

char *zombie(dir_t p)
{
  assert(DIR_OKAY(p));
  return zombie_poses[p]; /*since zombie has the same image as man */
}


