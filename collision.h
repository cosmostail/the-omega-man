/**********************************************************************
  Module: collision.h
  Author: Scott Wang
  Date:   2006 Oct 16
  Purpose: define all the collision events
**********************************************************************/
#ifndef COLLISION_H
#define COLLISION_H

#include "g_adt.h"

extern int collision_is_occur(game_type_t from, void *from_data,
			     game_type_t to, void *to_data);

#endif
