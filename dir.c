/**********************************************************************
  Module: dir.c
  Author: Jim Uhl
  Date:   2006 September 28

  Purpose: Direction data type implementation.
**********************************************************************/

#include <assert.h>
#include "dir.h"
#include "g_adt.h"
#include "field.h"

/* Order must match dir_t values. */
static const struct {
    int dx;
    int dy;
} dirs[] = {
    {  0, -1 },
    {  0,  1 },
    { -1,  0 },
    {  1,  0 },
};

/* Order must match dir_t values. */
static const char *dir_names[] = {
    "North",
    "South",
    "West",
    "East",
    "Dir#?"
};

int dir_is_legal(int row,int col,dir_t dir)
{
  dir_move(dir,&row,&col);

  return (FIELD_IS_IN_PLAY(row,col) 
	  && g_adt_get_type(row,col) == EMPTY);
}

dir_t dir_change_dir(int row,int col, dir_t dir)
{
  dir_t tmp = dir;
  int i;
  for(i=0;i < DIR_NUM; i++){
    dir = dir_clockwise(dir);
    if(dir_is_legal(row,col,dir))
       return dir;
  }
  return tmp;
}

void dir_move(dir_t dir, int *row, int *col) {
    assert(DIR_OKAY(dir));
    *row += dirs[dir].dy;
    *col += dirs[dir].dx;
}

dir_t dir_clockwise(dir_t dir) {
    static dir_t dir_next[] = {
	DIR_EAST,
	DIR_WEST,
	DIR_NORTH,
	DIR_SOUTH
    };

    assert(DIR_OKAY(dir));
    return dir_next[dir];
}

const char *dir_str(dir_t dir) {
    assert(DIR_OKAY(dir) || dir == DIR_NUM);
    return dir_names[dir];
}

