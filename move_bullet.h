/**********************************************************************
  Module: move_bullet.h
  Author: Scott Wang
  Date:   2006 October 07
  Purpose: move bullet 
**********************************************************************/
#ifndef MOVE_BULLET_H
#define MOVE_BULLET_H

#include "dir.h"
#include "g_adt.h"

/* Bullet speed */
#define BULLET_PRE_MOVE  75000
//#define BULLET_PRE_MOVE  1000

/* create a space for bullet argment */
extern void move_bullet_create(int row,int col, 
			              dir_t dir, game_type_t t);

extern void move_bullet(void *data);

/* remove a bullet on screen */
extern void move_bullet_killed(void *data);

#endif
