/**********************************************************************
  Module: main.c
  Author: Scott Wang 
  Date:   2006 Sep 29

  Purpose: Build a text-base game just like "attack"
**********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>
#include <string.h>
#include <curses.h>
#include <pthread.h>

#include "globals.h"
#include "clock.h"
#include "screen.h"
#include "image.h"
#include "field.h"
#include "move_man.h"
#include "move_zombie.h"
#include "move_bullet.h"
#include "g_adt.h"
#include "ethread.h"
#include "master.h"
#include "object_count.h"

/* define keystroke */
#define ESC	'\033'
#define UP 'w'
#define DOWN 's'
#define LEFT 'a'
#define RIGHT 'd'
#define BULLET ' '

int main()
{
  screen_init();
  screen_check();
  screen_draw_board();
  screen_refresh();

  /* initlize the object counter */
  object_count_init();

  /* set up master thread to deal with rebooting */
  master_create();

  /* set up abustruct data type for the game */
  g_adt_init();

  /* set up clock */
  clock_init();

  /* set up man */
  Man_args man_args = move_man_args_create();
  
  /* set up zombie */ 
  move_zombie_args_create();

  int c;
  while(1){  
    c = getch();
    switch (c) {
         case ESC:
	   pthread_mutex_lock(&curses_mutex);
	   screen_fini();
	   pthread_mutex_unlock(&curses_mutex);
           return 0;
         case UP:
	   move_man_change_pos(man_args,DIR_NORTH);
	   break;
         case DOWN:
	   move_man_change_pos(man_args,DIR_SOUTH);
	   break;
         case LEFT:
	   move_man_change_pos(man_args,DIR_WEST);
	   break;
         case RIGHT:
	   move_man_change_pos(man_args,DIR_EAST);
	   break;
         case BULLET:
	   move_man_lock();
	   move_bullet_create(move_man_get_row(man_args),
				move_man_get_col(man_args),
				move_man_get_dir(man_args),
				MAN_BULLET);
	   move_man_unlock();
	   break;
       }
  }
  return 0;
}
