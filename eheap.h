/**********************************************************************
  Module: eheap.h
  Author: Scott G Wang
  Date:   2006 Sep 16
  Purpose: Malloc a spaces..
  Modification History:
**********************************************************************/

#ifndef EHEAP_H
#define EHEAP_H

#include <stdlib.h>

void *emalloc(size_t nbytes);
void efree(void *p);

#endif
