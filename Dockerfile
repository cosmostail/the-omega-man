FROM debian

RUN apt-get update -y
RUN apt-get install build-essential libncurses5-dev libncursesw5-dev -y

WORKDIR /var/code
COPY . /var/code
RUN make clean && make
CMD ./attack
