# Instruction

![IMAGE ALT TEXT HERE](http://www.silvascreen.com/wp-content/uploads/SILCD1561-TheOmegaMan-cover.jpg)

Due to the use of biological weapons, all but one member of the human species has become a zombie. The remaining human must destroy as many zombies as possible using a specially crafted plasma gun, without touching any of them. At the start of the game, the human player starts in the south east corner of the remaining section of inhabitable world. Eight zombies enter, one at a time, in the north west corner and start wondering randomly through the inhabitable section of the world. The human must shoot as many zombies as possible before time runs out, or the player runs out of lives. The human and zombies can move either horizontally (east or wast) or vertically (north or south) in a given step and continue to move until blocked. In the case of the human, the human stops when it hits a wall, or loses a life if it hits a zombie. When a zombie collides with another zombie or a wall, it changes direction. When a zombie hits a shot, it is destroyed. When the human shoots, the plasma burst moves in the same direction the human is moving. The human is unaffected by plasma burst. The human has 2 minutes to complete the task, or succumbs to the biological pathogen and turns into a zombie, ending the game.

## How to Play
w for UP  
s for DOWN  
a for LEFT  
d for RIGHT  
space for BULLET   

## Download
```
docker pull cosmostail/the-omega-man:latest
```

## Play
```
docker run -it cosmostail/the-omega-man:latest
```
