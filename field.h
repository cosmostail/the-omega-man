/**********************************************************************
  Module: field.h
  Author: Jim Uhl
  Date:   2006 September 28

  Purpose: Useful constants and macros defining the game play area.

  Note: These values must match the initial playfield given in
        screen.c - in fact, the initial screen should really be drawn
        using the values given below.
  
  Modifier: Scott Wang
  Reason: changing the field file in order to fit my implementation
  
**********************************************************************/

#ifndef FIELD_H
#define FIELD_H


/* Macros defining the (logical) columns of the play area.  Note that
   FIELD_MIN_COL must currently be 0 - it is only named for symmetry.
*/
#define FIELD_MIN_COL 6
#define FIELD_MAX_COL 50
#define FIELD_WIDTH (FIELD_MAX_COL-FIELD_MIN_COL + 1)

/* Macros defining the (logical) rows of the play area.  Note that
   FIELD_MIN_ROW must currently be 0 - it is only named for symmetry.
*/
#define FIELD_MIN_ROW 1
#define FIELD_MAX_ROW 22
#define FIELD_HEIGHT (FIELD_MAX_ROW-FIELD_MIN_ROW + 1)

/* Evaluates to TRUE iff (row,col) is in the play area. */
#define FIELD_IS_IN_PLAY(row, col) \
    (   FIELD_MIN_COL <= (col) && (col) <= FIELD_MAX_COL \
     && FIELD_MIN_ROW <= (row) && (row) <= FIELD_MAX_ROW )

#endif /* FIELD_H */
