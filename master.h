/**********************************************************************
  Module: master.h
  Author: Scott Wang
  Date:   2006 Oct 5
  Purpose: A master thread to deal reboot the game
********************************************************************/

#ifndef MASTER_H
#define MASTER_H

extern void master_reset();
extern void master_create();
#endif
