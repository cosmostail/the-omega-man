/**********************************************************************
  Module: dir.h
  Author: Jim Uhl
  Date:   2006 September 28

  Purpose: Represent compass directions, and compute movements
           in each of these directions.

  Notes:   Passing an illegal direction to one of the functions will
           cause an assertion failure and the program to terminate.
**********************************************************************/

#ifndef DIR_H
#define DIR_H

/* The set of compass directions.  Names should be self-explanatory. */
typedef enum {
    DIR_NORTH,
    DIR_FIRST=DIR_NORTH,
    DIR_SOUTH,
    DIR_WEST,
    DIR_EAST,
    DIR_NUM
} dir_t;

/* True iff d is a legal direction. */
#define DIR_OKAY(d) ((d)>=DIR_FIRST && (d)<DIR_NUM)

/* Update *row and *col one step in the direction dir. */
extern void dir_move(dir_t dir, int *row, int *col);

/* Returns the direction 90 degrees clockwise from dir. */
extern dir_t dir_clockwise(dir_t dir);

/* For debugging!  Returns string representation of dir. */
extern const char *dir_str(dir_t dir);

/* return a legal position */
extern dir_t dir_change_dir(int row,int col, dir_t dir);

/* return true if the dir is legal */
extern int dir_is_legal(int row,int col,dir_t dir);

#endif /* DIR_H */
