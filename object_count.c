/**********************************************************************
  Module: object_count.c
  Author: Scott Wang
  Date:   2006 Nov 4
  Purpose: a counter that counts the objects numbers on screen
********************************************************************/
#include <pthread.h>
#include "object_count.h"

static int object_count;
pthread_mutex_t object_count_mutex = PTHREAD_MUTEX_INITIALIZER;
 
void object_count_init()
{
  object_count = 0;
}

void object_count_inc()
{
  pthread_mutex_lock(&object_count_mutex);
  object_count++;
  pthread_mutex_unlock(&object_count_mutex);
}

void object_count_dec()
{
  pthread_mutex_lock(&object_count_mutex);
  object_count--;
  pthread_mutex_unlock(&object_count_mutex);
}

int object_count_get()
{
  return object_count;
}
