/**********************************************************************
  Module: move_man.h
  Author: Scott Wang
  Date:   2006 Sep 29

  Purpose: To make man move in the given field
**********************************************************************/

#ifndef MOVE_MAN_H
#define MOVE_MAN_H

#include "dir.h"

#define MAN_PRE_MOVE 150000
//#define MAN_PRE_MOVE 1500
#define MAN_LIVE 3

typedef struct man_args *Man_args;

extern void move_man(void *data);
Man_args move_man_args_create();

/* change man's direction */
extern void move_man_change_pos(Man_args m, dir_t dir);

/* a man killed by zombie */
extern void move_man_killed(void *data);

/* return man's current row,col,and dir */
extern int move_man_get_row(Man_args m);
extern int move_man_get_col(Man_args m);
extern dir_t move_man_get_dir(Man_args m);

extern void move_man_lock();
extern void move_man_unlock();

#endif
