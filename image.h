/**********************************************************************
  Module: image.h
  Author: Scott Wang
  Date:   2006 Sep 29

  Purpose: Create game image
**********************************************************************/

#ifndef IMAGE_H
#define IMAGE_H

#include "dir.h"

#define MAN_WIDTH 1
#define MAN_HEIGHT 1

#define ZOMBIE_WIDTH 1
#define ZOMBIE_HEIGHT 1

#define BULLET_WIDTH 1
#define BULLET_HEIGHT 1
#define BULLET_IMAGE "*"

#define BARRIR_HEIGHT 1
#define BARRIR_IMAGE "#"

#define BARRIR_BLOCK_WIDTH 3
#define BARRIR_BLOCK_HEIGHT 2
#define EMPTY_BLOCK_WIDTH 3

#define MOD_WIDTH (BARRIR_BLOCK_WIDTH + EMPTY_BLOCK_WIDTH)
#define MOD_HEIGHT (BARRIR_BLOCK_HEIGHT + 1)

/* create man image */
char *man(dir_t p);

/* create zombie image */
char *zombie(dir_t p);

#endif
