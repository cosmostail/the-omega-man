/**********************************************************************
  Module: screen.c
  Author: Jim Uhl
  Date:   2004 October 02
  Mod:    2006 August 30
          2006 September 28

  Purpose: Initializes curses, draws 2d images on the screen, including
           gamescreen image.
**********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <curses.h>
#include <assert.h>
#include "screen.h"

#define ARRAY_SIZE(a) (sizeof(a)/sizeof(a[0]))
#define BUF_SIZE 10

static char *screen[] = {
  "     +---------------------------------------------+",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |    Attack Force",
  "     |                                             |",
  "     |                                             |    Time: ",
  "     |                                             |",
  "     |                                             |    Lives: n",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     |                                             |",
  "     +---------------------------------------------+",
};



void screen_init(void) {
    initscr();
    crmode();
    noecho();

    screen_draw_board();

    screen_refresh();
}

void screen_draw_board(void) {
    clear();
    screen_draw_image(0, 0, screen, ARRAY_SIZE(screen));
}

void screen_draw_image(int row, int col, char *image[], int height) {
    int i, length;
    int new_left, new_right, new_offset, new_length;

    assert(image != NULL);

    for (i = 0; i < height; i++) {
	if (row+i < 0 || row+i >= SCR_HEIGHT || image[i] == NULL)
	    continue;
	length = strlen(image[i]);
	new_left  = col < 0 ? 0 : col;
	new_offset = col < 0 ? -col : 0;
	new_right = col+length > SCR_WIDTH ? SCR_WIDTH-1 : col+length-1;
	new_length = new_right - new_left + 1;

	mvaddnstr(row+i, new_left, image[i]+new_offset, new_length);
    }
}

void screen_clear_image(int row, int col, int width, int height) {
    int i, j;
    
    for (i = 0; i < height; i++) {
	move(row+i, col);
	for (j = 0; j < width; j++)
	    addch(' ');
    }
}

void screen_refresh(void) {
    move(LINES-1, COLS-1);
    refresh();
}

void screen_bold_on(void) {
    attron(A_BOLD);
}

void screen_bold_off(void) {
    attroff(A_BOLD);
}

void screen_fini(void) {
    endwin();
}

void screen_check(void)
{
  if(LINES < SCR_HEIGHT || COLS < SCR_WIDTH){
    screen_fini(); 
    fprintf(stderr,"Screen size too small, minimum should be %d * %d\n",
	    SCR_WIDTH,SCR_HEIGHT);
  exit(1);
  }
}

void screen_draw_integer(int row, int col, int i)
{
  char buf[BUF_SIZE];
  snprintf(buf,(sizeof(buf)+1),"%d",i);
  mvaddstr(row,col,buf);
}
