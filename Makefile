######################################################################
#  File:   Makefile
#  Author: Jim Uhl
#  Date:   2004 October 05
#  Modified: 2005 October 01
#  Purpose: build up for Attack game 
######################################################################

LDLIBS=-lcurses -lpthread
CFLAGS=-pthread -g -Wall -D_XOPEN_SOURCE=600
LDFLAGS=-pthread -g

EXE=attack
OBJS=main.o move_bullet.o screen.o clock.o image.o eheap.o dir.o g_adt.o move_man.o ethread.o move_zombie.o collision.o master.o object_count.o

$(EXE): $(OBJS)
	$(CC) $(LDFLAGS) -o $(EXE) $(OBJS) $(LDLIBS)

main.o: main.c field.h image.h move_man.h screen.h

screen.o: screen.c screen.h

image.o: image.c image.h

move_man.o: move_man.c move_man.h image.h field.h eheap.h ethread.h globals.h

move_zombie.o: move_zombie.c move_zombie.h field.h eheap.h image.h

move_bullet.o: move_bullet.c move_bullet.h

eheap.o: eheap.c eheap.h

g_adt.o: g_adt.c g_adt.h field.h

dir.o: dir.c dir.h

clock.o: clock.c clock.h globals.h

collision.o: collision.h collision.c

ethread.o: ethread.h ethread.c

master.o: master.c master.h

object_count.o: object_count.c object_count.h

clean:
	rm -f core $(EXE) $(OBJS)
